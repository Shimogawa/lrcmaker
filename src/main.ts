import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import installElementPlus from "./plugins/element";
import "./assets/css/main.css";
import createI18n from "./i18n/i18n";
import useLrcMaker from "./lrcmaker/lrcm";

const app = createApp(App);

app
  .use(installElementPlus)
  .use(store)
  .use(createI18n)
  .use(useLrcMaker)
  .use(router)
  .mount("#app");
