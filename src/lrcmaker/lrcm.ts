import { App } from "vue";
import LMAudio from "./audio/lmaudio";
import LMLyricsController from "./lrc/lmlyrics";

export class LrcMaker {
  audio = new LMAudio();
  lyrics?: LMLyricsController;

  setPlainLyrics(textLyrics: string) {
    this.lyrics = new LMLyricsController(textLyrics);
  }

  // private readAudioFile() {
  //   this.checkAudioFile();
  //   const reader = new FileReader();
  //   reader.readAsText(this.audioFile!.raw, "utf-8");
  //   reader.onload = (e) => {
  //     console.log(e.target?.result);
  //   };
  // }
}

function useLrcMaker(app: App<Element>) {
  app.config.globalProperties.$lrcmaker = new LrcMaker();
}

export default useLrcMaker;
