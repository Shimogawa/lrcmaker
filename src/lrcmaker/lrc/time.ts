export default class TimeTag {
  value: number;

  constructor(value: number) {
    this.value = value;
  }

  toTag(): string {
    let secs = Math.floor(this.value);
    const mins = Math.floor(secs / 60);
    const milli = this.value - secs;
    secs = secs % 60;
    return `[${mins}:${secs}:${Math.round(milli * 100)}]`;
  }
}
