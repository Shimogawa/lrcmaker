import TimeTag from "./time";

class TextElement {
  text: string;
  startTime?: TimeTag;
  endTime?: TimeTag;

  constructor(text: string, startTime?: TimeTag, endTime?: TimeTag) {
    this.text = text;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  toLrc(): string {
    return `${this.startTime ? this.startTime.toTag() : ""}${this.text}${
      this.endTime ? this.endTime.toTag() : ""
    }`;
  }
}

class LMLyricsController {
  lyrics: TextElement[];

  constructor(lrc: string) {
    this.lyrics = [];
    for (const c of lrc) {
      this.lyrics.push(new TextElement(c));
    }
  }
}

export default LMLyricsController;
