import { APlayerOptions, Audio } from "aplayer";
import APlayer from "aplayer";

interface IFileWithRaw {
  raw: File;
  name: string;
  size: number;
}

class LMAudio {
  private audioFile?: IFileWithRaw;
  audio?: APlayer;

  setAudioFile(audioFile?: IFileWithRaw) {
    this.stop();
    this.audioFile = audioFile;
    // this.audio = <HTMLAudioElement>document.getElementById("audio"); // new Audio(URL.createObjectURL(this.audioFile!.raw));

    if (this.audio === undefined) {
      const options: APlayerOptions = {
        container: document.getElementById("aplayer")!,
        audio: [],
        volume: 0.5,
        loop: "none",
      };
      this.audio = new APlayer(options);
    }
    if (!audioFile) {
      this.audio!.list.clear();
      return;
    }
    const audio: Audio = {
      name: audioFile.name,
      artist: (audioFile.size / 1000).toFixed(0) + " kB",
      url: URL.createObjectURL(audioFile!.raw),
    };
    this.audio!.list.add(audio);
  }

  get hasAudioFile() {
    return this.audioFile !== undefined;
  }

  play() {
    this.audio?.play();
  }

  pause() {
    this.audio?.pause();
  }

  stop() {
    if (this.audio) {
      this.audio.pause();
      this.audio.audio.currentTime = 0;
    }
  }

  get isPlaying(): boolean {
    return !!this.audio?.audio.paused;
  }

  set playbackSpeed(speed: number) {
    if (this.audio) {
      this.audio.audio.playbackRate = speed;
    }
  }

  get playbackSpeed() {
    if (!this.audio) return 1;
    return this.audio?.audio.playbackRate;
  }

  private checkAudioFile() {
    if (this.audioFile === undefined) {
      throw new Error("No audio selected");
    }
  }
}

export default LMAudio;
