declare module "aplayer" {
  export default class APlayer {
    constructor(options?: APlayerOptions);

    static version: string;

    lrc: LrcControl;
    list: PlaylistControl;
    audio: HTMLMediaElement;

    play();
    pause();
    seek(time: number);
    toggle();
    on(event: string, handler: () => void);
    volume(percentage: number, nostorage: boolean);
    theme(color: string, index: number);
    setMode(mode: "mini" | "normal");
    skipBack();
    skipForward();
    destroy();
  }

  export type Control = {
    show();
    hide();
    toggle();
  };

  export interface LrcControl extends Control {}

  export interface PlaylistControl extends Control {
    add(audios: Array<Audio> | Audio);
    remove(index: number);
    switch(index: number);
    clear();
  }

  export type Audio = {
    name?: String;
    url?: String;
    artist?: String;
    cover?: String;
    lrc?: String;
    theme?: String;
    type?: "auto" | "hls" | "normal";
  };

  export type APlayerOptions = {
    container?: HTMLElement;
    audio?: Audio[];
    mini?: boolean;
    fixed?: boolean;
    loop?: "all" | "one" | "none";
    order?: "list" | "random";
    preload?: "none" | "metadata" | "auto";
    volume?: number;
    mutex?: boolean;
  };
}
