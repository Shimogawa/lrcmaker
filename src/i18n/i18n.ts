import { createI18n } from "vue-i18n";
import YAML from "yaml";

const ctx = require.context(".", true, /\.ya?ml$/);
const messages: { [key: string]: any } = {};
ctx.keys().forEach((key: string) => {
  const fileName = key.replace("./", "");
  const resource = ctx(key);
  const locale = fileName.replace(fileName.endsWith(".yaml") ? ".yaml" : ".yml", "");
  messages[locale] = YAML.parse(YAML.stringify(resource));
});

export default createI18n({
  locale: "en",
  fallbackLocale: "en",
  messages,
});
